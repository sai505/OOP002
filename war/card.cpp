#include "card.h"

Card::Card() {
	deck.reserve(52); //
	for (int r = Ace; r <= King; ++r) {
		for (int s = Hearts; s <= Spades; ++s) {
			Card c{ static_cast<Rank>(r), static_cast<Suit>(s) };
			deck.push_back(c);
		}
	}
}