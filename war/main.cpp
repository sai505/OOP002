//UAkron OOP Class: war Practice
//(c) Andrew Sutton, PhD
//All rights reserved
//Modification by SEUNG JUN LEE
//date: Sept 10, 2017.
//Do:
//1.Provide appropriate constructors and accessor functions as you see fit.
//2.In main.cpp, create a deck of 52 cards(use a vector), and initialized to one of the usual cards in a deck
//3.Initializing the deck of cards(using push_back, setting card members explicitly)
//4.Try to find a way to initialize the deck in a single line of code.

#include "card.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include <utility>     

int main()
{
	//Rank r1 = Jack;
	//Rank r2 = Ace;
	//std::cout << std::boolalpha << (r1 == r2) << std::endl; //PRINTS FALSE
	//*********************************************************
	//std::cout <<  (r1 == r2) << '\n'; //PRINTS 0, (AS FALSE)
	//std::cout << (r1 < r2) << '\n'; PRINTS 1, (AS TRUE)
	//*********************************************************

	// Widening conversion. Okay.
	//int n = Jack;
	//std::cout << n << '\n'; //PRINTS 11
	//*********************************************************
	//Widenig Conversion=Implicit Conversion=Correct by Complier=Throw error
	//int i {3.41}; //error
	//Rank r1{ Ace, Jack };//Error
	//*********************************************************

	// Narrowing conversion. Bad.
	// Rank r3 = 4; // error: cannot convert.
	//*********************************************************
	//Narrowing Conversion=Explicit Conversion=User is responsibility to lose data
	//int i = 3.44; //i=3, lost data
	//*********************************************************

	// Initialize some cards.
	//Card c1{ Ace, Spades };
	//Card c2{ Four, Hearts };

	// We should be able to do this.
	//Card c;

	// Declaration, invokes a constructor to initialize an object (c3).
	//Card c3 = c1;
	//*********************************************************
	//Card c4{ c3 }; //copy initialization
	//The default Copy Assignment (without Copy Operator) is memberwise copy= Disaster, ex) vector
	//It(default) makes objects points to the same element 
	//Soulution is: CopyConstructor(const Class_name& a) and Copy assignment which is 
	//Class_name& operator=(const Class_name& a)
	//However, it needs more memory allocation, so uses Moving Constructor with Moving Assignment
	//= Moving data from object to another = Migration 
	//Class_name(Class_name&& a); and Class_name& operator=(Class_name&& a); 
	//*********************************************************

	// Assignment (expression).
	//c1 = (c3 = c2);


	// Create a deck of cards.
	//std::vector<Card> deck;
	//deck.reserve(52); //
	//for (int r = Ace; r <= King; ++r) {
	//	for (int s = Hearts; s <= Spades; ++s) {
	//		Card c{ static_cast<Rank>(r), static_cast<Suit>(s) };
	//		deck.push_back(c);
	//	}
	//}

	// Create a deck of Cards 
	//*********************************************************
	Card init; //call default copy constructor that has a definition of initiallizing the deck of cards
	Card P1;
	P1.deck = std::move(init.deck);
	//*********************************************************

	// Range-base for loop.
	//for (Card c : deck) {
	//	std::cout << c.get_rank() << ' ' << c.get_suit() << '\n';
	//}

	//get_rank(&c);

	// The code above expands (roughly) to this:
	// for (auto iter = deck.begin(); iter != deck.end(); ++iter) {
	//   Card c = *iter;
	// }
}
