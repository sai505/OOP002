// (c) Andrew Sutton, PhD
// All rights reserved
// Added by SEUNG JUN LEE 
#pragma once
#include <utility>
#include <vector>

// Ranks of a card are:
// ace, 2-10, jack, king, and queen.

// Suits of a card are:
// hearts, diamonds, clubs, spades

// using Card = int[2];
// using Card = std::pair<int, int>;

enum Rank // An enumeration type
{
	Ace = 1, // An enumerator
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,

	Jack,
	Queen,
	King,
};

enum Suit
{
	Hearts,
	Diamonds,
	Clubs,
	Spades,
};
enum E {
	End
};
struct Card_Init{
	Rank *Rank_Ptr;
	Suit *Suit_Ptr;
};

// A playing card (in a standard deck) is a pair of rank and suit (see enums above).
class Card
{
public:
	// // Maybe not great. Creates a "not a card" abstraction.
	// Card()
	//   : rank(), suit()
	// { }

	// Possibly better. Creates an uninitialized value.
	// Card() { }
	Card();

	// Good. Make the compiler do the right thing.
	//Card() = default;

	// Construct a card with a rank and suit.
	Card(Rank r, Suit s)
		: rank(r), suit(s) // member initializer list
	{ }


	// // Copy constructor.
	// Card(const Card& c)
	//   : rank(c.rank), suit(c.suit)
	// { }

	// // Copy assignment operator.
	// Card& operator=(const Card& c)
	// {
	//   rank = c.rank;
	//   suit = c.suit;
	//   return *this;
	// }

	//Moving Constructor
	Card(Card&& a);
	Card& operator=(Card&& a);


	// Accessor functions.
	// Observer functions.

	Rank get_rank() const { return rank; }
	Suit get_suit() const { return suit; }
	std::vector<Card> deck;

private:
	Rank rank;
	Suit suit;
};

/*
Rank get_rank(const Card* this) {
	return this->rank;
}
*/

